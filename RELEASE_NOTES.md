Release Notes
-------------
1.2.0
-----
* Added a new common interface for dealing with formatters which need to return a Map instead of a Vector.

1.1.0
-----
* Added write method which makes this driver compatible with the new CommonInterface.

1.0.1
-----
* Fixed an issue with the decoder where it would encounter a key with \\ and fail.


1.0.0
-----
* Initial Release.
<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\format\driver\json
{
	use nuclio\core\plugin\Plugin;
	use nuclio\core\ClassManager;
	use nuclio\plugin\fileSystem\reader\FileReader;
	use nuclio\plugin\format\driver\common\CommonMapInterface;
	
	<<provides('format::json')>>	
	class JSON extends Plugin implements CommonMapInterface
	{
		const int SERVICES_JSON_SLICE			=1;
		const int SERVICES_JSON_IN_STR			=2;
		const int SERVICES_JSON_IN_ARR			=3;
		const int SERVICES_JSON_IN_OBJ			=4;
		const int SERVICES_JSON_IN_CMT			=5;
		const int SERVICES_JSON_LOOSE_TYPE		=16;
		const int SERVICES_JSON_SUPPRESS_ERRORS =32;
		const int SERVICES_JSON_USE_TO_JSON		=64;

		public static function getInstance(/* HH_FIXME[4033] */...$args):JSON
		{
			$instance=ClassManager::getClassInstance(self::class);
			return ($instance instanceof self)?$instance:new self();
		}

		/**
		 * reduce a string by removing leading and trailing comments and whitespace
		 *
		 * @param    $str    string     string value to strip of comments and whitespace
		 * @return   string  string     value stripped of comments and whitespace
		 * @access   private
		 */
		private function reduceString(string $str):string
		{
			$str=preg_replace
			(
				[
					// eliminate single line comments in '// ...' form
					'#^\s*//(.+)$#m',
					// eliminate multi-line comments in '/* ... */' form, at start of string
					'#^\s*/\*(.+)\*/#Us',
					// eliminate multi-line comments in '/* ... */' form, at end of string
					'#/\*(.+)\*/\s*$#Us'
				],
				'',
				$str
			);
			// eliminate extraneous space
			return trim($str);
		}

		public function read(string $filename, Map<arraykey,mixed> $options=Map{}):Map<string,mixed>
		{
			$reader=FileReader::getInstance('fileSystem::local-disk',$filename);
			$content=$reader->read();
			return $this->decode($content);
		}

		public function write(string $filename, Vector<mixed> $data, Map<arraykey,mixed> $options=Map{}):bool
		{
			$JSONString=json_encode($data);
			return (bool)file_put_contents($filename,$JSONString);
		}
		
		/**
		 * Decodes a JSON string into appropriate variable`
		 *
		 * @param    string   $str    JSON-formatted string
		 * @return   mixed    number, boolean, string, vector or map
		 *                    corresponding to given JSON input string.
		 *                    Note that decode() always returns strings
		 *                    in ASCII or UTF-8 format!
		 * @access   public
		 */
		public function decode(string $str):mixed
		{
			$str = $this->reduceString($str);

			switch (strtolower($str)) 
			{
				case 'true': 	return true;
				case 'false':  	return false;
				case 'null':    return null;
				default:
				{
					$m = array();
					
					if (is_numeric($str)) 
					{
						return ((float)$str == (int)$str) ? (int)$str : (float)$str;
					}
					elseif (preg_match('/^("|\').*(\1)$/s', $str, &$m) && $m[1] == $m[2]) 
					{
						// STRINGS RETURNED IN UTF-8 FORMAT
						$delim			=substr($str, 0, 1);
						$chrs			=substr($str, 1, -1);
						$utf8			='';
						$strlen_chrs	=strlen($chrs);
						for ($c = 0; $c < $strlen_chrs; ++$c)
						{
							$substr_chrs_c_2 = substr($chrs, $c, 2);
							$substr_chrs_c_3 = substr($chrs, $c, 3);
							$ord_chrs_c = ord($chrs[$c]);
							switch (true) 
							{
								case $substr_chrs_c_2 == '\b':
								{
									$utf8 .= chr(0x08);
									++$c;
									break;
								}
								case $substr_chrs_c_2 == '\t':
								{
									$utf8 .= chr(0x09);
									++$c;
									break;
								}
								case $substr_chrs_c_2 == '\n':
								{
									$utf8 .= chr(0x0A);
									++$c;
									break;
								}
								case $substr_chrs_c_2 == '\f':
								{
									$utf8 .= chr(0x0C);
									++$c;
									break;
								}
								case $substr_chrs_c_2 == '\r':
								{
									$utf8 .= chr(0x0D);
									++$c;
									break;
								}
								case $substr_chrs_c_2 == '\\':
								case $substr_chrs_c_2 == '\\"':
								case $substr_chrs_c_2 == '\\\'':
								case $substr_chrs_c_2 == '\\\\':
								case $substr_chrs_c_2 == '\\/':
								{
									if (($delim == '"' && $substr_chrs_c_2 != '\\\'')
									|| ($delim == "'" && $substr_chrs_c_2 != '\\"')
									|| ($delim == '"' && $substr_chrs_c_2 != '\\'))
									{
										$utf8 .= $chrs[++$c];
									}
									break;
								}
								case (bool)preg_match('/\\\u[0-9A-F]{4}/i', substr($chrs, $c, 6)):
								{
									// single, escaped unicode character
									$utf16 = chr(hexdec(substr($chrs, ($c + 2), 2))) . chr(hexdec(substr($chrs, ($c + 4), 2)));
									$utf8 .= iconv('UTF-16', 'UTF-8', $utf16);//$this->utf162utf8($utf16);
									$c += 5;
									break;
								}
								case (($ord_chrs_c >= 0x20) && ($ord_chrs_c <= 0x7F)):
								{
									$utf8 .= $chrs[$c];
									break;
								}
								case ($ord_chrs_c & 0xE0) == 0xC0:
								{
									// characters U-00000080 - U-000007FF, mask 110XXXXX

									//see http://www.cl.cam.ac.uk/~mgk25/unicode.html#utf-8
									$utf8 .= substr($chrs, $c, 2);
									++$c;
									break;
								}
								case ($ord_chrs_c & 0xF0) == 0xE0:
								{
									// characters U-00000800 - U-0000FFFF, mask 1110XXXX

									// see http://www.cl.cam.ac.uk/~mgk25/unicode.html#utf-8
									$utf8 .= substr($chrs, $c, 3);
									$c += 2;
									break;
								}
								case ($ord_chrs_c & 0xF8) == 0xF0:
								{
									// characters U-00010000 - U-001FFFFF, mask 11110XXX

									// see http://www.cl.cam.ac.uk/~mgk25/unicode.html#utf-8

									$utf8 .= substr($chrs, $c, 4);
									$c += 3;
									break;
								}
								case ($ord_chrs_c & 0xFC) == 0xF8:
								{
									// characters U-00200000 - U-03FFFFFF, mask 111110XX

									// see http://www.cl.cam.ac.uk/~mgk25/unicode.html#utf-8
									$utf8 .= substr($chrs, $c, 5);
									$c += 4;
									break;
								}
								case ($ord_chrs_c & 0xFE) == 0xFC:
								{
									// characters U-04000000 - U-7FFFFFFF, mask 1111110X

									// see http://www.cl.cam.ac.uk/~mgk25/unicode.html#utf-8
									$utf8 .= substr($chrs, $c, 6);
									$c += 5;
									break;
								}
							}
						}
						return $utf8;
					} 
					elseif (preg_match('/^\[.*\]$/s', $str) || preg_match('/^\{.*\}$/s', $str)) 
					{
						// array, or object notation
						$arr = Vector{};
						$obj = Map{};
						if ($str[0] == '[') 
						{
							$stk = array(self::SERVICES_JSON_IN_ARR);
							// $arr = Vector{};
						}
						else 
						{ 
							$stk = array(self::SERVICES_JSON_IN_OBJ);
							// $obj = Map{};
						}

						array_push
						(
							&$stk, 
							[
								'what'  => self::SERVICES_JSON_SLICE,
								'where' => 0,
								'delim' => false
							]
						);

						$chrs = substr($str, 1, -1);
						$chrs = $this->reduceString($chrs);

						if ($chrs == '') 
						{
							if (reset(&$stk) == self::SERVICES_JSON_IN_ARR) 
							{
								return $arr;
							}
							else 
							{
								return $obj;
							}
						}

						$strlen_chrs = strlen($chrs);
						
						for ($c = 0; $c <= $strlen_chrs; ++$c) 
						{
							$top = end(&$stk);
							
							$substr_chrs_c_2 = substr($chrs, $c, 2);
							if (($c == $strlen_chrs) || (($chrs[$c] == ',') && ($top['what'] == self::SERVICES_JSON_SLICE))) 
							{
								// found a comma that is not inside a string, array, etc.,
								// OR we've reached the end of the character list

								$slice = substr($chrs, $top['where'], ($c - $top['where']), );
								
								array_push(&$stk, array('what' => self::SERVICES_JSON_SLICE, 'where' => ($c + 1), 'delim' => false));

								if (reset(&$stk) == self::SERVICES_JSON_IN_ARR) 
								{
									// we are in an array, so just push an element onto the stack
									array_push(&$arr, $this->decode($slice));
								} 
								elseif (reset(&$stk) == self::SERVICES_JSON_IN_OBJ) 
								{
									// we are in an object, so figure out the property name

									$parts = array();

									// if (preg_match('/^\s*(["\'].*[^\\\]["\'])\s*:/Uis', $slice, $parts)) 
									if (preg_match('/^\s*(["\'].*["\'])\s*:/Uis', $slice, &$parts)) 
									{
										$key = $this->decode($parts[1]);
										$val = $this->decode(trim(substr($slice, strlen($parts[0])), ", \t\n\r\0\x0B"));
										// if ($this->use & self::SERVICES_JSON_LOOSE_TYPE) 
										// {
										$obj[$key] = $val;
										// } 
										//    else 
										//    {
										//     $obj->$key = $val;
										// }
									} 
									elseif (preg_match('/^\s*(\w+)\s*:/Uis', $slice, &$parts)) 
									{
										// name:value pair, where name is unquoted
										$key = $parts[1];
										$val = $this->decode(trim(substr($slice, strlen($parts[0])), ", \t\n\r\0\x0B"));
										// if ($this->use & self::SERVICES_JSON_LOOSE_TYPE) 
										//    {
										$obj[$key] = $val;
										// } 
										//    else 
										//    {
										//     $obj->$key = $val;
										// }
									}
								}
							} 
							elseif ((($chrs[$c] == '"') || ($chrs[$c] == "'")) && ($top['what'] != self::SERVICES_JSON_IN_STR)) 
							{
								// found a quote, and we are not inside a string
								array_push(&$stk, array('what' => self::SERVICES_JSON_IN_STR, 'where' => $c, 'delim' => $chrs[$c]));
							} 
							elseif (($chrs[$c] == $top['delim']) && ($top['what'] == self::SERVICES_JSON_IN_STR) && ((strlen(substr($chrs, 0, $c)) - strlen(rtrim(substr($chrs, 0, $c), '\\'))) % 2 != 1)) 
							{
								// found a quote, we're in a string, and it's not escaped
								// we know that it's not escaped becase there is _not_ an
								// odd number of backslashes at the end of the string so far
								array_pop(&$stk);
							} 
							elseif (($chrs[$c] == '[') && in_array($top['what'], array(self::SERVICES_JSON_SLICE, self::SERVICES_JSON_IN_ARR, self::SERVICES_JSON_IN_OBJ))) 
							{
								// found a left-bracket, and we are in an array, object, or slice
								array_push(&$stk, array('what' => self::SERVICES_JSON_IN_ARR, 'where' => $c, 'delim' => false));
							} 
							elseif (($chrs[$c] == ']') && ($top['what'] == self::SERVICES_JSON_IN_ARR)) 
							{
								// found a right-bracket, and we're in an array
								array_pop(&$stk);
							}
							elseif (($chrs[$c] == '{') && in_array($top['what'], array(self::SERVICES_JSON_SLICE, self::SERVICES_JSON_IN_ARR, self::SERVICES_JSON_IN_OBJ))) 
							{
								// found a left-brace, and we are in an array, object, or slice
								array_push(&$stk, array('what' => self::SERVICES_JSON_IN_OBJ, 'where' => $c, 'delim' => false));

							}
							elseif (($chrs[$c] == '}') && ($top['what'] == self::SERVICES_JSON_IN_OBJ)) 
							{
								// found a right-brace, and we're in an object
								array_pop(&$stk);
							}
							elseif (($substr_chrs_c_2 == '/*') && in_array($top['what'], array(self::SERVICES_JSON_SLICE, self::SERVICES_JSON_IN_ARR, self::SERVICES_JSON_IN_OBJ))) 
							{
								// found a comment start, and we are in an array, object, or slice
								array_push(&$stk, array('what' => self::SERVICES_JSON_IN_CMT, 'where' => $c, 'delim' => false));
								$c++;
							}
							elseif (($substr_chrs_c_2 == '*/') && ($top['what'] == self::SERVICES_JSON_IN_CMT)) 
							{
								// found a comment end, and we're in one now
								array_pop(&$stk);
								$c++;
								for ($i = $top['where']; $i <= $c; ++$i)
								{
									$chrs = substr_replace($chrs, ' ', $i, 1);
								}
							}
						}
						if (reset(&$stk) == self::SERVICES_JSON_IN_ARR) 
						{
							return $arr;
						}
						elseif (reset(&$stk) == self::SERVICES_JSON_IN_OBJ) 
						{
							return $obj;
						}
					}
				}
			}
		}
	}
}